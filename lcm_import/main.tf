#Dev branch main file from Test branch

module "environment" {
  source                            = "terraform.hosting.maersk.com/tsoe/terraform-azure-environment/azure"
  resource_group                    = var.resource_group_name
}

module "log_analytics" {
  source                            = "terraform.hosting.maersk.com/tsoe/terraform-azure-logana/azure"     
  resource_group                    = module.environment.resource_group_name
  log_analytics_workspace_name      = var.log_analytics_workspace_name
  solutions                         = [ "KeyVaultAnalytics", ]
}

module "keyvault" {
  source                            = "terraform.hosting.maersk.com/tsoe/terraform-azure-keyvault/azure"   
  vault_name                        = var.key_vault_name
  resource_group                    = module.environment.resource_group_name
  log_analytics_workspace_id        = module.log_analytics.id
  custom_tags                       = var.custom_tags
}

resource "azurerm_virtual_network" "vnet" {
  name                              = "${var.vm_group_name}-vnet"
  address_space                     = ["10.0.0.0/16"]
  location                          = module.environment.location
  resource_group_name               = module.environment.resource_group_name
}

resource "azurerm_subnet" "subnet" {
  name                              = "${var.vm_group_name}-subnet"
  resource_group_name               = module.environment.resource_group_name
  virtual_network_name              = "${azurerm_virtual_network.vnet.name}"
  address_prefix                    = "10.0.2.0/24"
}

module "virtual_machine" {
  source                            = "./../../../"
  n_vm                              = var.n_vm
  resource_group_name               = module.environment.resource_group_name
  vm_group_name                     = var.vm_group_name
  vm_size                           = var.vm_size
  custom_tags                       = var.custom_tags
  subnet_id                         = azurerm_subnet.subnet.id
  vm_private_ip_address_allocation  = var.vm_private_ip_address_allocation
  vm_data_disk_type                 = var.vm_data_disk_type
  vm_data_disk_size                 = var.vm_data_disk_size
  vm_aad_users                      = var.vm_aad_users
  vm_aad_admins                     = var.vm_aad_admins
  vm_admin_user_public_key          = var.vm_admin_user_public_key
  keyvault_id                       = module.keyvault.id
  log_analytics_workspace_name      = module.log_analytics.name
}
