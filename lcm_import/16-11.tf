locals {
  identity_map                      = length(var.user_assigned_identity_ids) >= 1 ? {type = "SystemAssigned, UserAssigned", ids = var.user_assigned_identity_ids} : {type = "SystemAssigned", ids = null}   
  av_zones                          = [1]
}

module "environment" {
  source                            = "terraform.hosting.maersk.com/tsoe/terraform-azure-environment/azure"
  version                           = "0.0.79242"
  resource_group                    = var.resource_group_name
}

provider "azurerm" {
  alias                             = "image_subscription"
  subscription_id                   = "89731d00-1ebf-435f-919e-45920d73b538"
}



module "log_analytics" {
  source                            = "terraform.hosting.maersk.com/tsoe/terraform-azure-logana/azure"
  resource_group                    = module.environment.resource_group_name
  log_analytics_workspace_name      = var.log_analytics_workspace_name
  solutions                         = [ "KeyVaultAnalytics", ]
}

module "keyvault" {
  source                            = "terraform.hosting.maersk.com/tsoe/terraform-azure-keyvault/azure"
  vault_name                        = var.key_vault_name
  resource_group                    = module.environment.resource_group_name
  log_analytics_workspace_id        = module.log_analytics.id
  custom_tags                       = var.custom_tags
}

resource "azurerm_virtual_network" "vnet" {
  name                              = "${var.vm_group_name}-vnetz"
  address_space                     = ["10.0.0.0/16"]
  location                          = module.environment.location
  resource_group_name               = module.environment.resource_group_name
}

resource "azurerm_subnet" "subnet" {
  name                              = "${var.vm_group_name}-subnetz"
  resource_group_name               = module.environment.resource_group_name
  virtual_network_name              = "${azurerm_virtual_network.vnet.name}"
  address_prefix                    = "10.0.2.0/24"
}


module "virtual_machine" {
  source                            = "./../../../"
  n_vm                              = var.n_vm
  resource_group_name               = module.environment.resource_group_name
  vm_group_name                     = var.vm_group_name
  vm_size                           = var.vm_size
  custom_tags                       = var.custom_tags
  subnet_id                         = azurerm_subnet.subnet.id
  vm_private_ip_address_allocation  = var.vm_private_ip_address_allocation
   vm_data_disk_type                 = var.vm_data_disk_type
   vm_data_disk_size                 = var.vm_data_disk_size
  vm_aad_users                      = var.vm_aad_users
  vm_aad_admins                     = var.vm_aad_admins
  vm_admin_user_public_key          = var.vm_admin_user_public_key
  keyvault_id                       = module.keyvault.id
  log_analytics_workspace_name      = module.log_analytics.name
  #num_interfaces                   = var.num_interfaces

}



module "oracle_managed_disk" {
  source                            = "terraform.hosting.maersk.com/tsoe/terraform-azure-managed-disk/azure"
  name                              = "${var.vm_group_name}-Oracle-datadisk"
  num_disks                         = var.n_data_disk
  resource_group_name               = module.environment.resource_group_name
  disk_type                         = var.vm_data_disk_type
  disk_size                         = var.vm_oracle_data_disk_size
  custom_tags                       = var.custom_tags
}

resource "azurerm_virtual_machine_data_disk_attachment" "oracle_data_disk" {
#  #vm_count                         = var.n_vm
  count                             = var.n_vm
  virtual_machine_id                = module.virtual_machine.ids[count.index]
  managed_disk_id                   = module.oracle_managed_disk.ids[count.index]
  lun                               = "${var.vm_data_disk_logic_unit_number + 1}"
  caching                           = var.vm_data_disk_caching_mode

}



#module "managed_disk" {
#  source                            = "terraform.hosting.maersk.com/tsoe/terraform-azure-managed-disk/azure"
#  #count                            = var.n_data_disk
#  name                              = "${var.vm_group_name}-datadiskz"
#  num_disks                         = var.n_data_disk
#  resource_group_name               = module.environment.resource_group_name
#  disk_type                         = var.vm_data_disk_type
#  disk_size                         = var.vm_data_disk_size
#  custom_tags                       = var.custom_tags
#}




#resource "azurerm_virtual_machine_data_disk_attachment" "data_disk" {
#  vm_count                         = var.n_vm
#  count                             = var.n_data_disk
#  virtual_machine_id                = "${element(module.virtual_machine.ids)}"
#  managed_disk_id                   = module.managed_disk.ids[count.index]
#  lun                               = "${(var.vm_data_disk_logic_unit_number) + count.index+1}"
#  caching                           = var.vm_data_disk_caching_mode
#}
